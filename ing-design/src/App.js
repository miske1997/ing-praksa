import React, { Component } from 'react';
import { faSearch ,faProjectDiagram, faTasks,faNewspaper,
faChartArea,faCheckDouble, faLightbulb, faFolderOpen,faHeart} from "@fortawesome/free-solid-svg-icons";
import { faInstagram , faFacebook ,faTwitter , faVimeo , faYoutube} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';

import Toolbar from './components/Toolbar/Toolbar';
import SideDrawer from './components/SideDrawer/SideDrawer';
import Backdrop from './components/Backdrop/Backdrop';
import Button from './components/Button/Button';
import MaskImg from './components/MaskImg/MaskImg';
import BpCard from './components/BpCard/BpCard';
import Item from './components/Item/Item';
import Coment from './components/Coment/Coment';
import Card from './components/Card/Card';
import PricingCard from './components/PricingCard/PricingCard';
import Post from './components/Post/Post';

import './App.css';
class App extends Component {
  state = {
    sideDrawerOpen: false
  };
  analyzeHandler = null;
  email = "";

  drawerToggleClickHandler = () => {
    this.setState((prevState) => {
      return {sideDrawerOpen: !prevState.sideDrawerOpen};
    });
  };

  backdropClickHandler = () => {
    this.setState({sideDrawerOpen: false});
  };

  render() {
    let backdrop;
    const bpText = "Nulla vitae elit libero,a pharetra augue..Donec id elit non mi porta gravida at eget metus.Cras justo."
    if (this.state.sideDrawerOpen) {
      backdrop = <Backdrop click={this.backdropClickHandler} />
    }
    if(this.email.match(/@/g) !== []){
      this.analyzeHandler = () => console.log(this.email);
    }
    else{
      this.analyzeHandler = null;
    }
    return (
      <div style={{height: '100%'}}>
        <Toolbar drawerClickHandler={this.drawerToggleClickHandler} />
        <SideDrawer show={this.state.sideDrawerOpen} />
        {backdrop}
        <main >
          <div className="home">
            <div className="head-line">
            <h3>Grow Your Business With <br /> Our Marketing Solutions</h3>
            <p> We Help our clients to increase their website <br /> traffic, rankings and visibility in search results </p>
            <Button text="TRY IT FOR FREE" ></Button>
            </div>
            <div>
              <img  className="rocket-stars" src={require("./assets/rocket3.png")} />
              <img  className="rocket-clouds" src={require("./assets/rocket4.png")} />
              <img  className="rocket" src={require("./assets/rocket2.png")} />
            </div>
            <img  className="rocket-cloud "src={require("./assets/rocket1.png")} />
          </div>
          <div className="what-we-do">
            <p>WHAT WE DO?</p>
            <h2>The full service we are offerig is specifically designed to meet your business needs.</h2>
            <div className="p-list">
              <BpCard icon={faSearch} title="SEO Service" text={bpText} bColor="rgb(243, 218, 225)"/>
              <BpCard icon={faNewspaper} title="Web Design" text={bpText} bColor="rgb(201, 224, 242)"/>
              <BpCard icon={faProjectDiagram} title="Social Engagment" text={bpText} bColor="rgb(225, 237, 212)"/>
              <BpCard icon={faChartArea} title="Content Marketing" text={bpText} bColor="rgb(224, 226, 238)"/>
            </div>
          <Item image="concept8.png" title="WHY CHOSE US?" row={true} text="Why is Search Engine Optimization important to your business"  >
            <p>{bpText}</p>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Duis a est vel mauris vehicula aliquam vitae posuere libero</span>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Vivamus iaculis nisi sit amet </span>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Aliquam euismod blandit ante</span>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Praesent a sollicitudin est.</span>
          </Item>
          <Item image="concept1.png" title="OUR PROCCES" row={false} text="We bring solutions to make life easier for our costomers"  >
            <Coment fa={faLightbulb} chip="1" color="rgb(157, 202, 233)" bColor="rgb(214, 230, 244)" title="Collect Ideas " text={bpText}></Coment>
            <Coment fa={faFolderOpen} chip="2" color="rgb(143, 204, 187)" bColor="rgb(211, 236, 230)" title="Data Analysis" text={bpText}></Coment>
            <Coment fa={faHeart} chip="3" color="rgb(249, 222, 177)" bColor="rgb(252, 238, 210)" title="Magic Touch" text={bpText}></Coment>
          </Item>
          <Item image="concept3.png" title="OUR PERSONALIZED SOLUTION" row={true} text="Just sit an relax while we take care of your business needs"  >
          <p>{bpText}</p>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Duis a est vel mauris vehicula aliquam vitae posuere libero</span>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Vivamus iaculis nisi sit amet </span>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Aliquam euismod blandit ante</span>
             <span> <FontAwesomeIcon icon={faCheckDouble} color="teal"/>&nbsp; Praesent a sollicitudin est.</span>
          </Item>
          </div>
          <div className="c-stories">
            <p>Customer Stories</p>
            <strong>Customer Satisfaction is our mayor goal. <br/> See what our costomers are saying about us.</strong>
            <div className="carousel-container">
              <Carousel  showThumbs={false} showIndicators={true} showStatus={false} width="100%" autoPlay={false} infiniteLoop={true} transitionTime={1000}>
                  <div >
                    <div className="slides">
                    <Card pic="customer-t1.jpg" title="Connor Gipson" text="Financial Analyst" comment="sadadasdasdasdasdasdasdas dasdasdsadadadasdadaa"/>
                    <Card pic="customer-t2.jpg" title="Coriss Ambady" text="Marketing Specialist" comment="sadadasdasdasdasdasdasdas dasdasdsadadadasdadaa"/>
                    <Card pic="customer-t3.jpg" title="Barclay Widerski" text="Sales Manager" comment="sadadasdasdasdasdasdasdas dasdasdsadadadasdadaa"/>   
                    </div>
                  </div>
                  <div >
                    <div className="slides">
                      <Card pic="customer-t4.jpg" title="Conor " text="asdasdasd" comment="sadadasdasdasdasdasdasdas dasdasdsadadadasdadaa"/>
                      <Card pic="customer-t5.jpg" title="agasfasfasfasfasgasga" text="asdasdasd" comment="sadadasdasdasdasdasdasdas dasdasdsadadadasdadaa"/>
                      <Card pic="customer-t6.jpg" title="agasfasfasfasfasgasga" text="asdasdasd" comment="sadadasdasdasdasdasdasdas dasdasdsadadadasdadaa"/>   
                    </div>
                  </div>
              </Carousel>
            </div>
          </div>
          <div className="pricing">
            <div className="pricing-description"> 
              <p>OUR PROCESS</p>
              <strong>We offer great prices, premium products and quality service for your business needs.</strong>
              <p> {bpText}</p>
            </div>
            <div className="card-container">
              <PricingCard pNmb="1" price="9" planTipe="Basic Plan"/>
              <PricingCard pNmb="5" price="19"  planTipe="Premium Plan"/>
            </div>
          </div>
          <div className="contact">
            <Item image="concept12.png" title="LET'S TALK" row={true} text="Let's make something great together.If you got any questions, don't hesitate to get in touch with us.">
              <p> {bpText} </p>
              <Button text="CONTACT US"></Button>
            </Item>
            <div>
              <p> ANALYZE NOW </p>
              <p> Wonder how much faster your web site can go? <br /> Easly Check your SEO Score now.</p>
              <div className="search-bar">
                <input type="text" maxLength="40" onChange={(event) => this.email = event.target.value}></input>
                <Button text="ANALYZE" click={this.analyzeHandler}/>
              </div>
              <img style={{position: "relative" , bottom: "-10px"}} src={require("./assets/rocket1.png")} />
            </div>
            <div className="footer">
              <div className="pop-posts">
              <p>Popular Posts</p>
                <Post title="Magna Mollis Ultricies" pic="footer-a1.jpg" date="12 NOV 2017" comments="4"/>
                <Post title="Ornare Nullam Risus" pic="footer-a2.jpg" date="12 NOV 2017" comments="4"/>
                <Post title="Euismod Nullam" pic="footer-a3.jpg" date="12 NOV 2017" comments="4"/>
              </div>
              <div className="tc">
                <p>Tags</p>
                <div className="tags">
                  <Button text="STILL LIFE"> </Button>
                  <Button text="NATURE"> </Button>
                  <Button text="URBAN"> </Button>
                  <Button text="LANDSCAPE"> </Button>
                </div>
                <p>Categories</p>
                <div className="categories">
                  <ul >
                    <li> <p>Lifestzle(21)</p> </li>
                    <li> <p>Photo(19)</p> </li>
                    <li> <p>Jurnal(16)</p> </li>
                    <li> <p>Works(7)</p> </li>
                    <li> <p>Still Life(9)</p> </li>
                    <li> <p>Travel(17)</p> </li>
                  </ul>
                </div>
              </div>
              <div className="ge">
                <div className="g-in-touch">
                  <p>Get in Toutch</p>
                  <p>Moonshine St. 14/15, Nis, Serbia</p>
                  <br />
                  <p>info@gmail.com <br/> +00(123) 456 78 90</p>
                </div>
                  <p>Elsewhere</p>
                <div className="elsewhere">
                    <a href="" ><FontAwesomeIcon icon={faTwitter} size="2x"/></a>
                    <a href="" ><FontAwesomeIcon icon={faFacebook} size="2x"/></a>
                    <a href="" ><FontAwesomeIcon icon={faInstagram} size="2x"/></a>
                    <a href="" ><FontAwesomeIcon icon={faVimeo} size="2x"/></a>
                    <a href="" ><FontAwesomeIcon icon={faYoutube} size="2x"/></a>
                </div>
              </div>
              <div className="lh">
                <div className="learn">
                  <p>Learn More</p>
                  <br />
                  <a href="" >About Us</a>
                  <a href="" >Our Story</a>
                  <a href="" >Projects</a>
                </div>
                <div className="help">
                  <p>Need Help?</p>
                  <br />
                  <a href="" >Support</a>
                  <a href="" >Get Started</a>
                  <a href="" >Contact Us</a>
                </div>
              </div>
            </div>
          </div>
        </main>
        
      </div>
    );
  }
}

export default App;
