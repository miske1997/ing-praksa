import React from 'react';
import './Card.css';

import MaskImg from "../MaskImg/MaskImg"

const card = (props) => (
    <div className="customer-card">
        <p>{props.comment}</p>
        <div>
            <MaskImg pic={props.pic}/>
            <div>
                <strong>{props.title}</strong>
                <p>{props.text}</p>
            </div>
        </div>
    </div>
);

export default card;