import React from 'react';

import './MaskImg.css';

const maskImg = (props) => {
    
    return (

    <div className="mask-img">
        <img  src={require(`../../assets/${props.pic}`)} style={{mask: "url('../../assets/blob2.svg')"}}/>
    </div>
    // style={{clipPath: require(`../../assets/blob4.svg`)}}
)};

export default maskImg;