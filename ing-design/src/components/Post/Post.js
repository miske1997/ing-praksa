import React from 'react';
import './Post.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock , faComment} from "@fortawesome/free-regular-svg-icons";

const post = (props) => (
    <div className="post">
        <img src={require(`../../assets/${props.pic}`)} />
        <div>
            <p> {props.title} </p>
            <p> <FontAwesomeIcon icon={faClock}/> {props.date} <FontAwesomeIcon icon={faComment}/> {props.comments} </p>
        </div>
    </div>
);

export default post;