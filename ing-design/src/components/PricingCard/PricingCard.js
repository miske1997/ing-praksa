import React from 'react';
import './PricingCard.css';

import Button from '../Button/Button';

const pricingCard = (props) => (
    <div className="pricing-card">
        <div className="pricing">
            <p>$</p> <p style={{fontSize: "38px"}}>{props.price}</p> <p>/month</p>
        </div>
        <div className="pricing-plan">
            <strong style={{paddingBottom: "40px"}}>{props.planTipe}</strong>
            <p><strong>{props.pNmb}</strong> {props.pNmb === "1" ? " Project" : " Projects"}</p>
            <p><strong>100K</strong>&nbsp; API Access</p>
            <p><strong> {props.mb}MB </strong> &nbsp; Storage</p>
            <p> Custom<strong>&nbsp;Cloud</strong>&nbsp;Services</p>
            <p> Weekly<strong>&nbsp;Reports</strong></p>
            <p><strong>7/24</strong>&nbsp;Suport</p>
            <Button text="CONTACT US" />
        </div>
    </div>

);

export default pricingCard