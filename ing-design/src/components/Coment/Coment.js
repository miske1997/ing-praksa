import React from 'react';
import './Coment.css';

import ChipImg from "../ChipImg/ChipImg"

const coment = (props) => (
    <div className="coment">
        <ChipImg icon={props.fa} chip={props.chip} color={props.color} bColor={props.bColor}/>
        <div className="coment-text">
            <strong>{props.title}</strong>
            <p>{props.text}</p>
        </div>
    </div>
);

export default coment;