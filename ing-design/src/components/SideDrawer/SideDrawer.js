import React from 'react';

import './SideDrawer.css';

const sideDrawer = props => {
  let drawerClasses = 'side-drawer';
  if (props.show) {
    drawerClasses = 'side-drawer open';
  }
  return (
    <nav className={drawerClasses}>
      <ul>
        <li>
          <p onClick={() => document.querySelector('.home').scrollIntoView({behavior: 'smooth' })}>HOME</p>
        </li>
        <li>
          <p onClick={() => document.querySelector('.what-we-do').scrollIntoView({behavior: 'smooth' })}>WHAT WE DO</p>
        </li>
        <li>
          <p onClick={() => document.querySelector('.c-stories').scrollIntoView({behavior: 'smooth' })}>CUSTOMER STORIES</p>
        </li>
        <li>
          <p onClick={() => document.querySelector('.pricing').scrollIntoView({behavior: 'smooth' })}>PRICING</p>
        </li>
        <li>
          <p onClick={() => document.querySelector('.contact').scrollIntoView({behavior: 'smooth' })}>CONTCAT US</p>
        </li>
      </ul>
    </nav>
  );
};

export default sideDrawer;
