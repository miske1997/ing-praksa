import React from 'react';

import './Button.css';

const button = (props) => (
    <button className="btn-s" onClick={props.click}>
        {props.text}
    </button>
);


export default button;