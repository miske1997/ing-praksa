import React from 'react';

import './Item.css';

const item = (props) => {
    
    let first = null;
    let second = null;
    if(props.row === true){
        first = (<img src={require(`../../assets/${props.image}`)} />);
        second = (<div>
            <p>{props.title}</p>
            <p className="item-text">{props.text}</p>
            {props.children}
        </div>);
    }
    else {

         second = (<img src={require(`../../assets/${props.image}`)} />);
         first = (<div>
            <p>{props.title}</p>
            <p className="item-text">{props.text}</p>
            {props.children}
        </div>);
    }
    return (

    <div className="item" style={{flexWrap: props.row === true ? "wrap-reverse" : "wrap"}}>
        {first}
        {second}
    </div>
)};


export default item;