import React from 'react'
import './ChipImg.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const chipImg = (props) => (
    <div className="chip-img">
        <div style={{backgroundColor: props.bColor , color: props.color}}>
            <div><FontAwesomeIcon icon={props.icon} size="3x"/></div>
        </div>
    <p style={{backgroundColor: props.color }}>{props.chip}</p>
    </div>
);


export default chipImg;