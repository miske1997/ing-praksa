import React from 'react';

import './BpCard.css';
import MaskImg from '../MaskImg/MaskImg';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const bpCard = (props) => (

    <div className="bp-card" >
        <div style={{backgroundColor: props.bColor, color: props.color}}> <FontAwesomeIcon icon={props.icon} size="2x"/>  </div>
        <h3>{props.title}</h3>
        <p>{props.text}</p>
    </div>

);

export default bpCard;