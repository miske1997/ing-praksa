import React from 'react';

import DrawerToggleButton from '../SideDrawer/DrawerToggleButton';
import './Toolbar.css';

const toolbar = props => (
  <header className="toolbar">
    <nav className="toolbar__navigation">
        <div className="toolbar__toggle-button">
            <DrawerToggleButton click={props.drawerClickHandler} />
        </div>
        <div className="spacer" />
        <img src={require('../../assets/logo.png')} />
        {/* <div className="toolbar__logo"><a href="/">LOGO</a></div> */}
        <div className="spacer" />
        <div className="toolbar_navigation-items">
            <ul>
              <li>
                <p onClick={() => document.querySelector('.home').scrollIntoView({behavior: 'smooth' })}>HOME</p>
              </li>
              <li>
                <p onClick={() => document.querySelector('.what-we-do').scrollIntoView({behavior: 'smooth' })}>WHAT WE DO</p>
              </li>
              <li>
                <p onClick={() => document.querySelector('.c-stories').scrollIntoView({behavior: 'smooth' })}>CUSTOMER STORIES</p>
              </li>
              <li>
                <p onClick={() => document.querySelector('.pricing').scrollIntoView({behavior: 'smooth' })}>PRICING</p>
              </li>
              <li>
                <p onClick={() => document.querySelector('.contact').scrollIntoView({behavior: 'smooth' })}>CONTCAT US</p>
              </li>
            </ul>
        </div>
        <div className="spacer" />
    </nav>
  </header>
);

export default toolbar;
